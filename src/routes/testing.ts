import { ObjectID } from "mongodb";
import { FastifyInstance } from "fastify";
import { shuffle } from "lodash";
import { partition, testing, accuracy, precision, recall, miscRate, testingAndChange } from "../services/testing";
import { summary } from '../services/summary';
import { Permintaan } from "@/models/Permintaan";
import * as assert from 'assert';

const ROUTE_PREFIX = "/testing";
type AsyncTask = () => Promise<any>;

export default async function (server: FastifyInstance, opts, next) {

  const collection = server.mongo.db.collection<Permintaan>("permintaan");

  server.post("/testing", async (request, reply) => {

    const k = parseInt(request.body.k);
    const dataset = request.body.dataset;

    let results: any[] = [];
    let items = await ( collection.find({ dataset }).toArray() );
    const parts = partition(items, k);

    for (let i = 0; i < k; i++) {
      // Declare the variables with 'let'. Capture the closure.
      let trainPart = trainingPart(parts, i);
      let testPart = parts[i];
      let summ = summary(trainPart);
      let partResult = await testing(summ, trainPart, testPart);
      results.push( partResult );
    }

    reply.send(results);
  });

  server.post("/testing-with-change", async (request, reply) => {
    
    const k = parseInt(request.body.k);
    const dataset = request.body.dataset;

    const findResult = await collection.find({ dataset });
    const items = await findResult.toArray();

    assert(items.length !== 0);

    const parts = partition(items, k);

    var currentTask: Promise<any> = Promise.resolve(1);

    parts.forEach((part, idx) => {
      // Capture the closures.
      let trainingData = parts.filter((p, pidx) => pidx !== idx)
      trainingData = trainingData.reduce((a, b) => a.concat(b), [])
      let summ = summary(trainingData);

      // Run each testing in order.
      currentTask = currentTask.then(() => testingUntilAccuracyIsGood(summ, trainingData, part, idx));
    });

    await currentTask;

    reply.send("OK");
  });


  async function testingUntilAccuracyIsGood(summ: any, training_part: any[], testing_part: any[], pidx: any) {
    let partResult = testing(summ, training_part, testing_part);
    if (partResult.accuracy < 65) {
      console.log(`[INFO] changing partition-${pidx}`);
      console.log(`[INFO] accuracy before changing partition-${pidx}: ${partResult.accuracy}`);
      await testingAndChange(summ, training_part, testing_part, changeFunc);
      partResult = testing(summ, training_part, testing_part);
      console.log(`[INFO] accuracy after changing partition-${pidx}: ${partResult.accuracy}`);
    }
    return partResult;
  }

  async function changeFunc (id, keterangan){
    const result = await collection.findOneAndUpdate(
      { _id: new ObjectID(id) }, 
      { $set: {
        keterangan
      } });
    assert(result.value !== null);
  }

  function trainingPart(parts, idxTrain) {
    return parts.filter((p, pidx) => pidx !== idxTrain).reduce((a, b) => a.concat(b), []);
  }
}